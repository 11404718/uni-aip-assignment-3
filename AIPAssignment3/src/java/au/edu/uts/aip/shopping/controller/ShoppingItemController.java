package au.edu.uts.aip.shopping.controller;

import au.edu.uts.aip.shopping.bean.ShoppingItem;
import java.io.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.validation.constraints.*;

@Named
@RequestScoped
public class ShoppingItemController implements Serializable {
	
	private String item;
	private String description;
	private double price;
	private int quantity;
	
	// Obtain the existing ShoppingListController.
	@Inject
	private ShoppingListController shoppingListController;
	
	
	// Add an item to the ShoppingListController.
	public String add() {
		
		// Build a new shopping item based on the passed data.
		ShoppingItem shoppingItem = new ShoppingItem(item, description, (int) (price * 100), quantity);
		
		
		// Add the built item to the ShoppingListController.
		shoppingListController.add(shoppingItem);
		
		
		// Redirect to the JSF homepage.
		return "home?faces-redirect=true";
		
	}
	
	@Size(min = 1)
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@Size(min = 1)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Min(1)
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Min(1)
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}

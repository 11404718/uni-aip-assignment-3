package au.edu.uts.aip.shopping.bean;

import java.io.*;
import java.text.*;
import javax.enterprise.context.*;
import javax.inject.*;

@Named
@RequestScoped
public class ShoppingItem implements Serializable {
	
	private String item;
	private String description;
	private int price;
	private int quantity;
	
	
	// Default constructor.
	public ShoppingItem() {
		super();
	}
	
	
	// Constructor with parameters for easier initialisation.
	public ShoppingItem(String item, String description, int price, int quantity) {
		this.item = item;
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}
	
	
	// Returns the total price of the shopping item by multiplying the price by the
	// requested quantity.
	public double getTotalPrice() {
		return (double) price * quantity;
	}
	
	
	// Returns the item's price in a formatted string type with decimals and a
	// dollar sign.
	public String getFormattedPrice() {
		
		// Convert the price integer into a double.
		double priceDouble = (double) price / 100;
		
		// Initialise the number formatter.
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		
		// Return the formatted currency.
		return formatter.format(priceDouble);
		
	}
	
	
	// Returns the item's price multiplied by the quantity in a formatted string type
	// with decimals and a dollar sign.
	public String getFormattedTotalPrice() {
		
		// Convert the price integer into a double.
		double totalPriceDouble = getTotalPrice() / 100;
		
		// Initialise the number formatter.
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		
		// Return the formatted currency.
		return formatter.format(totalPriceDouble);
		
	}
	
	
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
package au.edu.uts.aip.shopping.controller;

import au.edu.uts.aip.shopping.bean.*;
import java.io.*;
import java.util.*;
import javax.ejb.*;
import javax.enterprise.context.*;
import javax.inject.*;

@Named
@Stateless
public class ShoppingListController implements Serializable {
	
	ArrayList<ShoppingItem> items = new ArrayList<ShoppingItem>();
	private boolean showForm = false;
	
	// Add a shopping item to the list.
	public void add(ShoppingItem shoppingItem) {
		items.add(shoppingItem);
	}
	
	
	// Setter method which allows for the user to use example items if requested.
	public void setExampleItems() {
		
		// Set the class's items to example items.
		this.items = getExampleItems();
		
	}

	
	// Automatically generates example items for use in demonstration.
	public ArrayList<ShoppingItem> getExampleItems() {
		
		// Initialise the temporary shopping list.
		ArrayList<ShoppingItem> exampleItems = new ArrayList<ShoppingItem>();
		
		
		// Create a series of example items.
		ShoppingItem exampleItem1 = new ShoppingItem("Apple", "Fuji Apples", 250, 5);
		ShoppingItem exampleItem2 = new ShoppingItem("Banana", "Wax Tipped Bananas", 120, 5);
		ShoppingItem exampleItem3 = new ShoppingItem("Milk", "Lite Milk 2L", 200, 2);
		ShoppingItem exampleItem4 = new ShoppingItem("Yoghurt", "Yoplait Vanilla", 560, 3);
		ShoppingItem exampleItem5 = new ShoppingItem("Bread", "Multigrain Sandwich", 320, 1);
		
		
		// Add the example items to the temporary list.
		exampleItems.add(exampleItem1);
		exampleItems.add(exampleItem2);
		exampleItems.add(exampleItem3);
		exampleItems.add(exampleItem4);
		exampleItems.add(exampleItem5);
		
		
		// Return the built example list.
		return exampleItems;
		
	}
	
	
	// Removes all items from the items list.
	public String removeAll() {
		
		// Remove all items from the shopping list.
		items.removeAll(items);
		
		
		// Kill the current session.
		SessionController.killSession();
		
		
		// Redirect to JSF homepage.
		return "/jsf/home.xhtml?faces-redirect=true";
		
	}
	
	
	// Returns the relevant form text depending whether the form is being
	// shown or not.
	public String getFormText() {
		return showForm ? "Close form..." : "Add item to the list...";
	}
	
	
	// Show the add form if it is hidden, and vice versa.
	public void invertShowForm() {
		showForm = !showForm;
	}
	
	
	// Returns whehter or not the list contains items.
	public boolean getHasItems() {
		return !items.isEmpty();
	}
	
	
	public ArrayList<ShoppingItem> getItems() {
		return items;
	}
	
	public void setItems(ArrayList<ShoppingItem> items) {
		this.items = items;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	
}

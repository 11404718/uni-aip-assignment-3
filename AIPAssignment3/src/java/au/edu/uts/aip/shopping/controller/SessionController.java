package au.edu.uts.aip.shopping.controller;

import java.io.*;
import javax.enterprise.context.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.servlet.http.*;

@Named
@RequestScoped
public class SessionController implements Serializable {
	
	// Initialise the static user session variable
	static HttpSession userSession;
	
	// Kill the current session if it exists.
	public static void killSession() {

		// Store the existing session into the userSession variable.
		userSession = getSession();
		
		
		// If session is not null, kill the session.
		if (userSession != null) {
			userSession.invalidate();
		}
		
	}
	
	
	// Returns the current session.
	public static HttpSession getSession() {
		
		// Get the current request from the Faces Context.
		FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		
		// Store the current session in the class' session variable.
		return request.getSession();
		
	}
	
	
}

package au.edu.uts.aip.shopping.controller;

import java.io.*;
import javax.enterprise.context.*;
import javax.inject.*;

@Named
@SessionScoped
public class HomeController implements Serializable {
	
	// Obtain the existing ShoppingListController.
	@Inject
	private ShoppingListController shoppingListController;
	
	public String navigateJSP() {
		
		// Kill the current session.
		SessionController.killSession();
		
		//
		return "jsp/home.jsp?faces-redirect=true";
		
	}
	
	public String navigateJSF() {
		
		// Kill the current session.
		return shoppingListController.removeAll();
		
	}

	
}

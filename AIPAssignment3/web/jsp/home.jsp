<%@page import="java.util.*"%>
<%@page import="au.edu.uts.aip.shopping.bean.*"%>
<%@page import="au.edu.uts.aip.shopping.controller.*"%>
<jsp:useBean id="shoppingListController" class="au.edu.uts.aip.shopping.controller.ShoppingListController" scope="session" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<%@include file="head.html" %>
    </head>
    <body>
		<%@include file="pageheader.html" %>
		<h2>Producing the shopping list using JavaServer Pages.</h2>
		<%			
			
			// Check whether the shopping list contains items.
			if (shoppingListController.getHasItems()) {

		%>		
		<table class="shoppingListTable">
			<thead>
			<th>Item</th>
			<th>Description</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total Price</th>
		</thead>
		<tbody>
			<%
				
				// Output each item within the shopping list, with a foreach loop
				// generating each row and each cell manually using their
				// corresponding HTML tags.
				for (ShoppingItem shoppingItem : shoppingListController.getItems()) {

					out.print("<tr>");

					out.print("<td>");
					out.print(shoppingItem.getItem());
					out.print("</td>");

					out.print("<td>");
					out.print(shoppingItem.getDescription());
					out.print("</td>");

					out.print("<td>");
					out.print(shoppingItem.getFormattedPrice());
					out.print("</td>");

					out.print("<td>");
					out.print(shoppingItem.getQuantity());
					out.print("</td>");

					out.print("<td>");
					out.print(shoppingItem.getFormattedTotalPrice());
					out.print("</td>");

					out.print("</tr>");

				}

				%>
		</tbody>
	</table>
		<br>
		<p><a href="add.jsp">Add item to the list</a></p>
			<%

			} else {

			%>
			<p>There are currently no items in the shopping list.</p><br>
			<p><a href="add.jsp">Add item to the list</a></p>
			<p><a href="generate.jsp">Generate example items</a></p>
			<%
			}
			%>
	</body>
</html>
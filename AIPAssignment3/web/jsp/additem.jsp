<%@page import="au.edu.uts.aip.shopping.bean.*"%>
<jsp:useBean id="shoppingListController" class="au.edu.uts.aip.shopping.controller.ShoppingListController" scope="session" />
<%
	
	try {
	
		// Receive item details from POST parameters.
		String name = request.getParameter("item");
		String description = request.getParameter("description");
		int price =  (int) (Double.parseDouble(request.getParameter("price")) * 100);
		int quantity = Integer.parseInt(request.getParameter("quantity"));

		
		// Build a new shopping item to add to the list.
		ShoppingItem shoppingItem = new ShoppingItem(name, description, price, quantity);

		
		// Add the new shopping item to the list.
		shoppingListController.add(shoppingItem);

		
		// Redirect to the homepage.
		String redirectURL = "/AIPAssignment3/faces/jsp/home.jsp";
		response.sendRedirect(redirectURL);
	
	} catch (NullPointerException e) {
		
		// Alert the user that not all parameters were provided.
		out.print("Not all parameters provided.");
		
	}
	
%>
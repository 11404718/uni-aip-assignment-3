<jsp:useBean id="shoppingListController" class="au.edu.uts.aip.shopping.controller.ShoppingListController" scope="session" />
<%

	// Add example items to the Shopping List Controller.
	shoppingListController.setExampleItems();
	
	// Redirect to the homepage.
	String redirectURL = "/AIPAssignment3/faces/jsp/home.jsp";
    response.sendRedirect(redirectURL);
	
%>
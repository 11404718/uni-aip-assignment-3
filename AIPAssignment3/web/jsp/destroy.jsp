<%@page import="au.edu.uts.aip.shopping.controller.SessionController"%>
<%

	// Kills the existing session.
	SessionController.killSession();
	
	// Redirect to the homepage.
	String redirectURL = "/AIPAssignment3/faces/jsp/home.jsp";
    response.sendRedirect(redirectURL);
	
%>
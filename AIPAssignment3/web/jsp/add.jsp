<%@page import="java.util.*"%>
<%@page import="au.edu.uts.aip.shopping.bean.*"%>
<%@page import="au.edu.uts.aip.shopping.controller.*"%>
<jsp:useBean id="shoppingListController" class="au.edu.uts.aip.shopping.controller.ShoppingListController" scope="session" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<%@include file="head.html" %>
    </head>
    <body>
		<%@include file="pageheader.html" %>
		<h2>Add an item to the shopping list using JSP.</h2>
		<br>
		<form action="additem.jsp" method="POST">
			<table class="form">
				<tr>
					<td>Item</td>
					<td><input name="item" type="text" required /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><input name="description" type="text" required /></td>
				</tr>
				<tr>
					<td>Price</td>
					<td><input name="price" type="number" min="1" step="0.01" required /></td>
				</tr>
				<tr>
					<td>Quantity</td>
					<td><input name="quantity" type="number" min="1" required /></td>
				</tr>
				<tr>
					<td></td>
					<td><input name="item" type="submit" /></td>
				</tr>
			</table>
		</form>
	</body>
</html>